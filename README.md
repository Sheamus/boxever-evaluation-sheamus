# Alright Flights

## About this project
- Project scaffolded with the Yeoman fullstack generator to get the app up and running quick

- Uses Bootstrap and UI Bootstrap
- UI Router for switching between views
- Express server returns flights from RESTful endpoint

- I've made use of ECMAScript 6 classes with constructors - a great addition to the language!
- Bit of javascsript map() and filter() from ECMAScript 5 for a little bit of functional programming
- Used some Lambdas
- ECMAScript 6 Promises got a look in too
- RESTful ajax call to get airports

Look under /client/app for controllers and services
Karma test under /client/app/airports

## Getting Started

### Prerequisites

- [Git](https://git-scm.com/)
- [Node.js and npm](nodejs.org) Node ^4.2.3, npm ^2.14.7
- [Bower](bower.io) (`npm install --global bower`)
- [Ruby](https://www.ruby-lang.org) and then `gem install sass`
- [Grunt](http://gruntjs.com/) (`npm install --global grunt-cli`)

### Developing

1. Run `npm install` to install server dependencies.

2. Run `bower install` to install front-end dependencies.

3. Run `grunt serve` to start the development server. It should automatically open the client in your browser when ready.

## Build & development

Run `grunt build` for building and `grunt serve` for preview.

## Testing

Running `npm test` will run the unit tests with karma.
