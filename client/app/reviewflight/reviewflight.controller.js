'use strict';

class ReviewFlightCtrl {

  constructor($state, order) {
    this.$state = $state;
    this.customerdetails = order.customerdetails;
    this.flight = order.flight;
  }

  bookFlight() {
    this.$state.go('summary');
  }
}
angular.module('alrightFlightsApp')
  .controller('ReviewFlightCtrl', ReviewFlightCtrl);
