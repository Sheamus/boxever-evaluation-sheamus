'use strict';

angular.module('alrightFlightsApp')
  .config(function($stateProvider) {
    $stateProvider
      .state('reviewflight', {
        url: '/reviewflight',
        templateUrl: 'app/reviewflight/reviewflight.html',
        controller: 'ReviewFlightCtrl',
        controllerAs: 'reviewflight'
      });
  });
