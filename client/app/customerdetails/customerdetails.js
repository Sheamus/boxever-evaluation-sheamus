'use strict';

angular.module('alrightFlightsApp')
  .config(function($stateProvider) {
    $stateProvider
      .state('customerdetails', {
        url: '/customerdetails',
        templateUrl: 'app/customerdetails/customerdetails.html',
        controller: 'CustomerDetailsCtrl',
        controllerAs: 'customerdetails'
      });
  });
