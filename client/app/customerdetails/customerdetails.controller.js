'use strict';

class CustomerDetailsCtrl {

  constructor($state, order) {
    this.$state = $state;
    this.details = order.customerdetails;
  }

  /**
   * Mark the form as submitted so errors are displayed
   * and only go to the next screen if form is valid
   * @param detailsForm
     */
  submitDetails(detailsForm) {
    detailsForm.$setSubmitted();
    if (detailsForm.$valid) {
      this.$state.go('reviewflight');
    }
  }
}
angular.module('alrightFlightsApp')
  .controller('CustomerDetailsCtrl', CustomerDetailsCtrl);
