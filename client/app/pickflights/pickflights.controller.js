'use strict';

class PickflightsCtrl {

  constructor($state, flightsearch, order) {
    this.$state = $state;
    this.order = order;
    this.flightsearch = flightsearch;
    this.search = order.search;
    this.flight = order.flight;

    this.results = [];
    this.searchFlights()
  }

  searchFlights() {
    var _this = this;
    this.flightsearch.searchFlights(this.search)
      .then(function(response) {
        _this.results.push.apply(_this.results, response);
      });
  }

  hasFlight() {
    return this.flight.depart !== undefined;
  }

  chooseFlight(index) {
    this.order.flight = this.results[index];
    this.order.flight.from = this.search.from;
    this.order.flight.to = this.search.to;
    this.order.flight.start = this.search.start;
    this.order.flight.finish = this.search.finish;
    this.$state.go('customerdetails');
  }
}
angular.module('alrightFlightsApp')
  .controller('PickflightsCtrl', PickflightsCtrl);
