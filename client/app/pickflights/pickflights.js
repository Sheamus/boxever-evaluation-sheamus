'use strict';

angular.module('alrightFlightsApp')
  .config(function($stateProvider) {
    $stateProvider
      .state('pickflights', {
        url: '/pickflights',
        templateUrl: 'app/pickflights/pickflights.html',
        controller: 'PickflightsCtrl',
        controllerAs: 'pickflights'
      });
  });
