'use strict';
/**
 * Represents the current customer state
 * including their search, flight selection
 * and order
 */
class OrderService {

  constructor() {
    this._search = {};
    this._search.start = moment().toDate();
    this._search.finish = moment().add(1, 'days').toDate();
    this._customerdetails = {};
    this._flight = {};
  }

  get search() {
    return this._search;
  }

  set search(n) {
    this._search = n
  }

  get customerdetails() {
    return this._customerdetails;
  }

  set customerdetails(n) {
    this._customerdetails = n
  }

  get flight() {
    return this._flight;
  }

  set flight(n) {
    this._flight = n
  }

}
angular.module('alrightFlightsApp')
  .service('order', OrderService);
