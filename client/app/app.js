'use strict';

angular.module('alrightFlightsApp', [
  'alrightFlightsApp.constants',
  'ngCookies',
  'ngResource',
  'ngSanitize',
  'angularMoment',
  'ui.router',
  'ui.bootstrap'
])
  .config(function($urlRouterProvider, $locationProvider) {
    $urlRouterProvider
      .otherwise('/');

    $locationProvider.html5Mode(true);
  });
