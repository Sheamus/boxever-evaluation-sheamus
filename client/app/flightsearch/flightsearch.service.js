'use strict';
class FlightSearchService {

  constructor() {
    this.flights = [
      {
        'depart': '7:40',
        'arrive': '18:30',
        'number': 'QF540',
        'economy': 330,
        'business': 450
      },
      {
        'depart': '8:30',
        'arrive': '13:45',
        'number': 'QF120',
        'economy': 340,
        'business': 490
      },
      {
        'depart': '10:20',
        'arrive': '18:00',
        'number': 'QF950',
        'economy': 430,
        'business': 600
      },
      {
        'depart': '11:20',
        'arrive': '18:30',
        'number': 'QF180',
        'economy': 220,
        'business': 360
      },
      {
        'depart': '13:10',
        'arrive': '19:30',
        'number': 'QF83',
        'economy': 320,
        'business': 460
      },
      {
        'depart': '15:40',
        'arrive': '18:50',
        'number': 'QF743',
        'economy': 430,
        'business': 820
      },
      {
        'depart': '16:40',
        'arrive': '21:30',
        'number': 'QF403',
        'economy': 605,
        'business': 900
      },
      {
        'depart': '17:10',
        'arrive': '12:15',
        'number': 'QF324',
        'economy': 637,
        'business': 999
      },
      {
        'depart': '7:40',
        'arrive': '18:30',
        'number': 'QF540',
        'economy': 330,
        'business': 450
      }
    ]
  }

  searchFlights(searchParams) {
    let _this = this;
    var promise = new Promise(function(resolve, reject) {

      if (searchParams.from && searchParams.to) {
        let resultCount = Math.floor(Math.random() * 6) + 2;
        resolve(_this.flights.slice(0, resultCount));
      }
      else {
        reject(Error("It broke"));
      }
    });
    return promise;
  }

}
angular.module('alrightFlightsApp')
  .service('flightsearch', FlightSearchService);
