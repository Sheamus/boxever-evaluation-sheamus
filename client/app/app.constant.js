(function(angular, undefined) {
'use strict';

angular.module('alrightFlightsApp.constants', [])

.constant('appConfig', {userRoles:['guest','user','admin']})

;
})(angular);