'use strict';

(function() {

class MainController {

  constructor($state, moment, airports, order) {
    this.$state = $state;
    this.airports = airports;
    this.search = order.search;
    this.minDate = moment();
    this.order = order;

    this.startPopup = {
      opened: false
    };

    this.finishPopup = {
      opened: false
    };
  }

  /**
   * Open start date popup
   */
  openStart() {
    this.startPopup.opened = true;
  }

  /**
   * Open return date popup
   */
  openFinish() {
    this.finishPopup.opened = true;
  }

  /**
   * Call airports service which uses ajax to search
   * for matching airports
   * @param val
   * @returns {*}
     */
  getLocation(val) {
    return this.airports.getAirportsByCityName(val)
      .then(function(response){
        return response.data.map(function(item){
          return item.name + ' (' + item.iata + ')';
        });
      });
  }

  /**
   * Set the search on the order service
   * and then...
   * Send the user to the next step
   * @param val
     */
  searchFlights() {
    this.order.flight = {};
    this.search = {
      'from': this.from,
      'to': this.to,
      'start': this.start,
      'finish': this.finish
    };
    this.$state.go('pickflights');
  }

}

angular.module('alrightFlightsApp')
  .controller('MainController', MainController);

})();
