'use strict';
class AirportsService {

  constructor($http) {
    this.$http = $http;
  }

  getAirportsByCityName(address) {
    return this.$http.get('/api/airports', {
      params: {
        address: address
      }
    });
  }

}
angular.module('alrightFlightsApp')
  .service('airports', AirportsService);
