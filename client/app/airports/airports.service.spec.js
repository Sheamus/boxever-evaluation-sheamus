'use strict';

describe('Service: Airports', function () {
  var airports, httpBackend;

  beforeEach(module('alrightFlightsApp'));

  beforeEach(inject(function (_airports_, $httpBackend) {
    airports = _airports_;
    httpBackend = $httpBackend;
  }));


  it("should pass through IATA codes", function () {
    httpBackend.whenGET("/api/airports?address=syd").respond(
      function () {
        return [
          /*status*/ 200,
          /*data*/
          [{
            "iata": "TJP",
            "lon": "-66.563545",
            "iso": "PR",
            "status": 1,
            "name": "Areopuerto Internacional Michael Gonzalez",
            "continent": "NA",
            "type": "airport",
            "lat": "18.010702",
            "size": "large"
          },
            {
              "iata": "AMC",
              "lon": "20.283333",
              "iso": "MX",
              "status": 1,
              "name": null,
              "continent": "NA",
              "type": "airport",
              "lat": "11.033333",
              "size": "large"
            },
            {
              "iata": "POM",
              "lon": "147.21446",
              "iso": "PG",
              "status": 1,
              "name": "Port Moresby Jacksons International Airport",
              "continent": "OC",
              "type": "airport",
              "lat": "-9.444308",
              "size": "large"
            },
            {
              "iata": "KEF",
              "lon": "-22.624283",
              "iso": "IS",
              "status": 1,
              "name": "Keflavik International Airport",
              "continent": "EU",
              "type": "airport",
              "lat": "63.997765",
              "size": "large"
            }]
        ];
      }
    );
    airports.getAirportsByCityName("syd").then(function(val) {
      expect(val.data[0].iata).toEqual("TJP");
    });
    httpBackend.flush();
  });

});

