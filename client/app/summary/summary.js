'use strict';

angular.module('alrightFlightsApp')
  .config(function($stateProvider) {
    $stateProvider
      .state('summary', {
        url: '/summary',
        templateUrl: 'app/summary/summary.html',
        controller: 'SummaryCtrl',
        controllerAs: 'summary'
      });
  });
