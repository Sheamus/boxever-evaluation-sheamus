'use strict';

class SummaryCtrl {

  constructor($state, order) {
    this.$state = $state;
    this.customerdetails = order.customerdetails;
    this.flight = order.flight;
    this.bookingreference = this.createBookingReference();
  }

  createBookingReference() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i=0;i < 5;i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }
}
angular.module('alrightFlightsApp')
  .controller('SummaryCtrl', SummaryCtrl);
