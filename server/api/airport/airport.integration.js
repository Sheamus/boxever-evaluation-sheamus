'use strict';

var app = require('../..');
import request from 'supertest';

describe('Airport API:', function() {

  describe('GET /api/airports', function() {
    var airports;

    beforeEach(function(done) {
      request(app)
        .get('/api/airports')
        .expect(200)
        .expect('Content-Type', /json/)
        .end((err, res) => {
          if (err) {
            return done(err);
          }
          airports = res.body;
          done();
        });
    });

    it('should respond with JSON array', function() {
      airports.should.be.instanceOf(Array);
    });

  });

});
