'use strict';

var proxyquire = require('proxyquire').noPreserveCache();

var airportCtrlStub = {
  index: 'airportCtrl.index'
};

var routerStub = {
  get: sinon.spy()
};

// require the index with our stubbed out modules
var airportIndex = proxyquire('./index.js', {
  'express': {
    Router: function() {
      return routerStub;
    }
  },
  './airport.controller': airportCtrlStub
});

describe('Airport API Router:', function() {

  it('should return an express router instance', function() {
    airportIndex.should.equal(routerStub);
  });

  describe('GET /api/airports', function() {

    it('should route to airport.controller.index', function() {
      routerStub.get
        .withArgs('/', 'airportCtrl.index')
        .should.have.been.calledOnce;
    });

  });

});
